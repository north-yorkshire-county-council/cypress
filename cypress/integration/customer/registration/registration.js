describe('#1 - Account Registration should be accessible from the front page', function() {
  it('Criteria 1 - Checks there\'s a registration button', function() {
    // Given - I have navigated to the portal front page.
    cy.visit('/')

    // Then - There should be an option to register for the portal
    cy.get('div#block-views-register-block-block').within(() => {
      cy.get('a.btn-primary').contains("Register")
    });

  })
  it('Criteria 2 - Checks I can go to the registration page', function() {
    // Given - I have navigated to the portal front page.
    cy.visit('/')

    // When - I click on the registration button
    cy.get('div#block-views-register-block-block').within(() => {
      cy.get('a.btn-primary').contains("Register").click()
    });

    // Then - I should be taken to the registration page.
    cy.url().should('include', '/register')
    cy.get('h1').should('contain', 'egist')

  })
});
describe('#2 - Registration should be explained before I start', function() {
  it('Criteria 1 - Checks there\'s explanation text', function() {
    // Given - I have navigated to the portal front page.
    cy.visit('/')

    // Then -  there should be text which explains the benefits of an account on this page.
    cy.get('div#block-views-register-block-block').within(() => {
      cy.get('p').contains("With an account")
    });

  })
  it('Criteria 2 - Checks there\'s a link to the privacy policy', function() {
    // Given - I have navigated to the portal front page.
    cy.visit('/')

    // When - I read the registration information
    cy.get('div#block-views-register-block-block').within(() => {
      // Then - There should be a link to the privacy policy
      cy.get('a').contains("privacy").should('have.attr', 'target', '_blank')
    });


  })
  it('Criteria 3 - Checks the current privacy policy opens in a new tab', function() {
    // Given - I have navigated to the portal front page.
    cy.visit('/')

    // When - I click on the registration button
    cy.get('div#block-views-register-block-block').within(() => {
      cy.get('a').contains("privacy").should('have.attr', 'target', '_blank')
      // Then - I should be redirected to the current privacy policy in a new tab
      cy.get('a').contains("privacy").click()
    });
  })
})
describe('#3 - Registration page design', function() {
  it('Criteria 1 - Checks we\'re explaining why we need the info', function() {
    // Given -  that I have browsed to the Account registration page
    cy.visit('/user/register')

    // Then - TEach of the semantically distinct fields should have preface text explaining why we need the information
      // Names
    cy.get('div.field-name-field-portal-first-name').within(() => {
      cy.get('div.description')
    });
      // DoB
    cy.get('div.row').contains("Date of birth").parent().parent().parent().within(() => {
      cy.get('span.description')
    });
    // Address
  cy.get('div.portal-address-helper').within(() => {
    cy.get('div.description')
  });
  // Email
cy.get('div#edit-account').within(() => {
  cy.get('div.description')
});
// Alerts Signup
cy.get('div.form-item-EmailAlertsOptIn').within(() => {
cy.get('div.description')
});
// Alerts Signup
cy.get('div.form-item-terms').within(() => {
cy.get('div.description')
});



// And - the Email alerts section should be optional.

// And - there should be a link to the terms and conditions of account creation

  })
  it('Criteria 2 - Checks that the T&Cs works', function() {
    // Given - that I have browsed to the Account registration page
    cy.visit('/user/register');

    // When - I click on the terms and conditions link
    cy.get('a').contains("terms and conditions").click();

    // Then -  I should be redirected to the corporate website terms-and-conditions page.
    cy.get('a').contains("terms and conditions").should('have.attr', 'target', '_blank');
    //https://www.northyorks.gov.uk/terms-and-conditions
    cy.get('a').contains("terms and conditions").should('have.attr', 'href')
        .and('include', 'https://www.northyorks.gov.uk/terms-and-conditions')
  })
});
