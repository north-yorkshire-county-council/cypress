### Story  
<!--- Provide a few word summary of the story in the Title above --->

<!--- Replace <role>, <short feature description>  and <value it adds> below with your story elements! --->
As a ‹role›, I'd like to ‹short feature description›, in order to ‹value it adds›. 

### Context

<!--- Provide any additional contextual information about the story here --->

### Acceptance criteria (Conditions of satisfaction)

1. [ ] High-level condition that should be satisfied 
2. [ ] High-level condition that should be satisfied
3. [ ] …

### Scenarios (Acceptance tests)

* 1.1 Given - Application context which must be present in order to test the criteria/condition 
    * When -  The specific Action which must be performed in order to test the criteria/condition 
    * Then -  The specific response which should result from the above action. 
 
* 2.1 Given - ...
    * When - ...
    * Then - ...

* 3.1 Given - ...
    * When - ...
    * Then - ...
